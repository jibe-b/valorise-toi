
## Écrire des tests ou pas ?

Pour être mergée, une merge request doit contenir des tests unitaires.

Afin de permettre un merge rapide, pensez à écrire les tests avant d'écrire le code, à la façon du Test Driven Development.

## Contact

Bon code à toutes et à tous, n'hésitez pas me poster des issues avec des questions, j'y répondrai volontiers !
