# Valorise-toi

## Description

Outil d'exécution de tests sur des répos git, donnant lieu à un badge PASS+nom-du-test associé à ce répo.

Les tests doivent déterminer une unique condition liée à une compétence. Par exemple "ce code est fonctionnel pur".

Cet outil permet ainsi de :
- faire grandir un _book_ (décentralisé) de compétences de développement d'un individu
- tenir un registre (centralisé) de bouts de code correspondant à un critère et pouvant servir d'exemple (par exemple, un registre de bouts de code fonctionnels purs)

Le nom est insipiré d'un document produit au sein de l'association Scouts et Guides de France.

## Design

Ce répo _Valorise-toi_ contient des tests, ainsi qu'une liste de répos déclarant souhaiter être testés. Un répo souhaitant être testé doit contenir un fichier valorise-toi.yml à sa racine.

Un fichier gitlab-ci.yml associé à un runner gitlab décrit l'exécution des tests, et l'écriture des résultats dans un des dossiers du répo.

1. Nouveau répo

Le runner exécute les tests correspondants au répo ajouté par une pull request ou un push.

Le fichier gitlab-ci.yml exécute la séquence suivante :
- faire un diff de la liste des répos souhaitant être testés et parser le nouveau répo
- cloner ce répo
- parser le fichier valorise-toi.yml et obtenir le test souhaité et le language, ainsi que les fichiers à tester, ainsi que d'autres informations (email, etc)
- exécuter le test
- écrire dans un dossier de résultats si le test PASSes ou FAILs
- crée un badge correspondant
- notifie par email l'auteur (email de l'authentification git{lab|hub} du résultat du test
- si le test PASSes, ajoute le répo à la liste des illustrations d'un test

2. Répos existants

Dans la mesure de la disponibilité du runner, les tests sont répétés pour tenir à jour le badge (et notifier l'auteur·e en cas de changement de PASS à FAIL).

L'étape de clonage du répo est suivie par une étape d'évaluation des changements à la rsync, afin de ne réexécuter le test que si le ou les fichiers à tester ont été modifiés récemment.


## Usage des résultats

L'auteur·e d'un répo peut ajouter le badge dans le README.md de son répo, ou tenir un book des compétences confrontées aux tests.

## Contribuer

Toutes les contributions sont les bienvenues ! Un petit coup d'œil au fichier [CONTRIBUTING.md](CONTRIBUTING.md) vous donnera les clefs :)
